void shuffle(Swappable s) {
  Random r = new Random();
  for(int i = 0; i < s.size(); i++) {
    s.swap(i, r.nextInt(s.size()));
  }
}
