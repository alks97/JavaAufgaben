int mod(int a, int b) {
  if(a < b) return a;
  return mod(a - b, b);
}
