class SwappableList2<E> extends ArrayList<E> implements Swappable {
  public void swap(int i, int j) {
    E temp = get(i);
    set(i, get(j));
    set(j, temp);
  }
}

SwappableList2<Integer> lst = new SwappableList2<>();
lst.add(1); lst.add(2); lst.add(3); lst.add(4);
shuffle(lst);
printf(lst.toString());
