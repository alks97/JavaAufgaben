import java.text.MessageFormat;
import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

class Track implements Comparable<Track> {
  private static final Pattern EXTINF_PATTERN = Pattern.compile(
    "#EXTINF:([\\d]+),([^\\-]+)\\-(.+)");
  String filename;
  int number;
  String artist;
  String title;
  public Track(String filename, String extinf) {
    this.filename = filename;
    Matcher m = EXTINF_PATTERN.matcher(extinf);
    if(m.matches()) {
      this.number = Integer.parseInt(m.group(1).trim());        // <1>
      this.artist = m.group(2).trim();                          // <1>
      this.title = m.group(3).trim();                           // <1>
    } else {
      this.number = 0;
      this.artist = "unknown Artist";
      this.title = "unknown Title";
    }
  }
  public int compareTo(Track other) {
    if(other.artist.equals(this.artist)) {
      return this.title.compareTo(other.title);
    } else {
      return this.artist.compareTo(other.artist);
    }
  }
  public String toString() {
    return MessageFormat.format(
      "#EXTINF:{0}, {1} - {2}\n{3}",
      number,
      artist,
      title,
      filename
    );
  }
}

List<Track> readPlaylist(String inName) throws IOException {
  Charset utf8 = StandardCharsets.UTF_8;
  List<Track> lst = new ArrayList<>();
  try(BufferedReader br = Files.newBufferedReader(Paths.get(inName), utf8)) {
    if(!br.readLine().startsWith("#EXTM3U")) {
      throw new IllegalArgumentException("File has no #EXTM3U header");
    }
    String line;
    String extinf = "";
    while((line = br.readLine()) != null) {
      if(line.startsWith("#EXTINF")) {
        extinf = line;
      } else if (line.trim().length() > 0) {
        lst.add(new Track(line, extinf));
        extinf = "";
      }
    }
  }
  return lst;
}

void writePlaylist(List<Track> playlist, String outName) throws IOException {
  Charset utf8 = StandardCharsets.UTF_8;
  try(BufferedWriter br = Files.newBufferedWriter(Paths.get(outName), utf8)) {
    br.write("#EXTM3U\n");
    for(Track t : playlist) {
      br.write(t.toString());
      br.write("\n");
    }
  }
}

List<Track> pl = readPlaylist("D:\\temp\\test.m3u");
Collections.sort(pl);
writePlaylist(pl, "D:\\temp\\test.m3u");
