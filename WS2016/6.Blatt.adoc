= 6. Woche: Fragen und Aufgaben zu OOP
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V0.9, 2016-11-18
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/6
:imagedir: ./images

////
Neu sollten Aufgaben hinzukommen, bei denen die Vererbung eine Rolle
spielt.


Wir können immer Aufgaben gebrauchen, die das algorithmische Denken
trainieren. Das kann unter "Gemischte Aufgaben" eingeordnet werden.
////


== Gemischte Aufgaben

=== Wieso ist der Code korrekt für negative b?

Der nachfolgende Code implementiert die Addition einzig durch Inkrementieren und Dekrementieren der Summanden. Dass der Code für positive Werte für `b` funktioniert, ist offensichtlich. Aber arbeitet er auch korrekt für negative Werte von `b`? Und wenn ja: Warum?

[source,java]
----
int add(int a, int b) {
    while(b-- != 0) a++;
    return a;
}
----

ifdef::solution[]
.Lösung
image::{imagedir}\AddIncDec.png[]
endif::solution[]

=== Vergleich von Zeichenketten

Ein Student kommt mit einem Problem zu mir:

====
Ich lege eine String-Variable an und teste, ob die Zeichenkette leer ist. Es ergibt sich das erwartete Ergebnis:
----
jshell> String test = ""
test ==> ""

jshell> test == ""
$2 ==> true
----

Erzeuge ich eine leere Zeichenkette über den folgenden Umweg, funktioniert der Vergleich nicht mehr. Was ist da los?
----
jshell> test = "a".substring(1)
test ==> ""

jshell> test == ""
$4 ==> false
----
====

Können Sie helfen?

ifdef::solution[]
.Lösung
Zeichenketten, Strings, sind Referenztypen. Java garantiert, dass String-Literale (wie z.B. `"Hello"`) ebenso wie Strings als Ergbnis konstanter Ausdrücke (wie z.B. `"Hel" + "lo"`) intern von ein und derselben String-Instanz stammen. Aus diesem Grund funktioniert in solchen Fällen ein Vergleich mit dem `==`-Operator.
----
jshell> "Hello" == "Hel" + "lo"
$6 ==> true
----

Der Gebrauch von `substring` erzwingt die Erzeugung einer neuen, von der leeren Zeichenkette verschiedenen String-Instanz. Da der Gleich-Operator bei Objekten nur feststellt, ob die Referenzen der Objekte identisch sind, werden ansonsten inhaltsgleiche Zeichenketten durchaus als "ungleich" erkannt. Wenn Sie die Inhalte der Zeichenketten vergleichen wollen, müssen Sie die Methode `.equals` verwenden.
----
jshell> "a".substring(1) == ""
$21 ==> false

jshell> "a".substring(1).equals("")
$22 ==> true
----

IMPORTANT: Vergleiche Zeichenketten stets mit der Methode `.equals`, nicht jedoch mit dem Vergleichsoperator `==`.
endif::solution[]

////
Sortierbare Punkte mit Comparable ("natural ordering")
Gleichheit von Punkten/Autos/Brüchen (equals)
sumFromTo, range mit exceptions
  - illegal argument
Integer.parseInt - exception fangen (mit JOptionPane?)

Zwei klassen, gemeinsamkeiten zu Interface machen.
Finalize überschreiben in eigener Klasse um zu sehen wann objekt gelöscht wird
Beispiel von Hierarchie, wie ist vererbung aufzubauen bzw wie kann vererbung aufgebaut werden? Ist eine komplexe Zahl auch eine natürliche Zahl? Ist eine ganze Zahl eine natürliche Zahl?
  - Tiere?
  - Zahlmengen
  - oder Schachfiguren
Mischbarer Kartenstapel erbt von Stack
  - Zusatzmethoden: deal(x) teilt x karten als Stack oder Array aus
  - auf pers. aufteilen
Einheiten mit Spezialfähigkeiten bei Partially Accurate battle simulator
  - multiplicator nicht als einzelne methode, sondern überschrieben für jede Klasse
Beispielcode ohne Vererbung, umstrukturieren mit vererbung
Wolf-Ziege-Kohl bzw. Missionare und Kannibalen mit NomNomException
Division durch 0 abfangen in div-Methode (exceptions)
  - bei fraction
Wechselgeld herausgeben mit Exceptions
Iterable Stack
  - foreach

Nadja:

Arithmetische Ausdrücke (bzw. andere einfache Grammatik) bauen
  - Term als interface
  - Add
  - Mul
  - Number
  - eval und toString als Methoden

////
