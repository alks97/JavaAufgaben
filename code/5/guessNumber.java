class Quizz {

    int min = 1, max, guess;

    Quizz(int maxNumber) {
        if (maxNumber < min)
            throw new IllegalArgumentException("n should be >= " + min);
        max = maxNumber;
        guess = guess();
    }

    private int guess() {
        return (max + min) / 2;
    }

    public String ask() {
        if (min == max) return "The number is " + guess;
        return "Is the number <= " + guess + "?";
    }

    public String answer(boolean yes) {
        if (yes) max = guess;
        else min = guess + 1;
        guess = guess();
        return ask();
    }
}
