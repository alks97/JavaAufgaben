short[] getBlock(short[] field, int nr) {
  int blockX = nr % 3;
  int blockY = nr / 3;
  short[] res = new short[9];
  for(int i = 0; i < 9; i++) {
    int x = blockX * 3 + i % 3;
    int y = blockY * 3 + i / 3;
    int idx = y * 9 + x;
    res[i] = field[idx];
  }
  return res;
}

short[] field = new short[81];
for(short i = 0; i < field.length; i++) {
  field[i] = i;
}
getBlock(field, 0);