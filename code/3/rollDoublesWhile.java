int rollDoubles() {
  Random r = new Random();
  int x = 0;
  int y = -1;                     // <1>
  int i = 0;
  while(x != y) {
    x = r.nextInt(6) + 1;
    y = r.nextInt(6) + 1;
    i++;
  }
  return i;
}