void printArray2D(int[] a, int w) {
  for(int i = 0; i < a.length; i++) {
    if(i > 0 && i % w == 0) System.out.printf("\n");
    System.out.printf("%2d ", a[i]);
  }
  System.out.printf("\n");
}

int[] test = {
  -1, 2, 0,
   3,-5, 6,
  -1, 0,-2
};
