boolean isInteger(String s) {
  try {
    int number = Integer.parseInt(s);
    return true;                            // <1>
  } catch (NumberFormatException nfe) {
    return false;
  }
}